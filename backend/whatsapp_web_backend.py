#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function;
import sys;
sys.dont_write_bytecode = True;

import signal;
import os;
import base64;
import time;
import json;
import uuid;
import traceback;
import re;

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket;
from whatsapp import WhatsAppWebClient;
from utilities import *;

reload(sys);
sys.setdefaultencoding("utf-8");

def keyboardInterruptHandler(signal, frame):
    print ("KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal));
    server.close();
    time.sleep(0.3);
    exit(0);

def eprint(*args, **kwargs):			# from https://stackoverflow.com/a/14981125
	s = "\x1b[94m";
	for arg in args:
		s = s + str(arg) + " ";
	if kwargs:
		s = s + str(kwargs);
	s = s + "\x1b[0m";
	print(s, file=sys.stderr, **kwargs);

def normalizePhone(phone):
	m = re.match(r'(\+?)(0?)(\d+)(@c.us)', phone);
	if m:
		s = "";
		g = m.groups();
		if not g[0]:
			s = s + "+";
		if g[1]:
			s = s + "62";
		s = s + g[2];
		if not g[3]:
			s = s + '@c.us';
		return s;
	return None;

conns = [];

class WhatsAppWeb(WebSocket):
	clientInstances = {};

	def sendJSON(self, obj, tag=None):
		if "from" not in obj:
			obj["from"] = "backend";
		eprint("sending " + json.dumps(obj));
		if tag is None:
			tag = str(getTimestampMs());
		self.sendMessage(tag + "," + json.dumps(obj));

	def sendError(self, reason, tag=None):
		eprint("sending error: " + reason);
		self.sendJSON({ "type": "error", "reason": reason }, tag);

	def handleMessage(self):
		try:
			eprint(self.data);
			tag = self.data.split(",", 1)[0];
			obj = json.loads(self.data[len(tag)+1:]);

			eprint(obj);
			if "from" not in obj or obj["from"] != "api2backend" or "type" not in obj or not (("command" in obj and obj["command"] == "backend-connectWhatsApp") or "whatsapp_instance_id" in obj):
				self.sendError("Invalid request, required parameters not found!");
				return;

			if obj["type"] == "call":
				if "command" not in obj:
					self.sendError("Invalid request, command not found!");
					return;

				if obj["command"] == "backend-connectWhatsApp":
					# Restore live session by whatsapp_instance_id
					if "whatsapp_instance_id" in obj and obj["whatsapp_instance_id"] and obj["whatsapp_instance_id"] in self.clientInstances:

						# Jika WhatsApp sudah login, maka [clientToken, serverToken, browserToker] pasti ada
						params = { "resource_instance_id" : obj["whatsapp_instance_id"] };
						if self.clientInstances[obj["whatsapp_instance_id"]].connInfo:
							eprint("Cek Instance {}: ".format(obj["whatsapp_instance_id"]), self.clientInstances[obj["whatsapp_instance_id"]].connInfo);

							if self.clientInstances[obj["whatsapp_instance_id"]].connInfo["clientToken"]:
								params["clientToken"] = self.clientInstances[obj["whatsapp_instance_id"]].connInfo["clientToken"];
								ok = True;

						if ok:
							self.sendJSON(mergeDicts({ "type": "resource_reattached", "resource": "whatsapp" }, params));
							return;
						else:
							self.sendError("Session not found.");
							return;

					# Restore live session by clientId, clientToken, browserToken maupun nomor telepon
					if any(k in obj for k in ("clientId", "clientToken", "browserToken", "phone")):
						ok = None;
						if "phone" in obj:
							phone = normalizePhone(obj["phone"]);
							if not phone:
								obj.pop("phone");
							else:
								obj["phone"] = phone;

						for i in self.clientInstances:
							if "clientId" in obj and obj["clientId"] == self.clientInstances[i].loginInfo["clientId"]:
								ok = i;
								break;
							elif "clientToken" in obj and obj["clientToken"] == self.clientInstances[i].connInfo["clientToken"]:
								ok = i;
								break;
							elif "browserToken" in obj and obj["browserToken"] == self.clientInstances[i].connInfo["browserToken"]:
								ok = i;
								break;
							elif "phone" in obj and obj["phone"] == self.clientInstances[i].connInfo["me"]:
								ok = i;
								break;
						if ok is not None:
							params = {
								"type": "resource_reattached",
								"resource": "whatsapp",
								"resource_instance_id" : ok,
								"clientToken": self.clientInstances[i].loginInfo["clientToken"]
							};
							self.sendJSON(params);
							return;
						else:
							self.sendError("Session not found.");
							return;

					# Create new session
					clientInstanceId = uuid.uuid4().hex;
					onOpenCallback = {
						"func": lambda cbSelf: self.sendJSON(mergeDicts({ "type": "resource_connected", "resource": "whatsapp" }, getAttr(cbSelf, "args")), getAttr(cbSelf, "tag")),
						"tag": tag,
						"args": { "resource_instance_id": clientInstanceId }
					};
					onMessageCallback = {
						"func": lambda obj, cbSelf, moreArgs=None: self.sendJSON(mergeDicts(mergeDicts({ "type": "whatsapp_message_received", "message": obj, "timestamp": getTimestampMs() }, getAttr(cbSelf, "args")), moreArgs), getAttr(cbSelf, "tag")),
						"args": { "resource_instance_id": clientInstanceId }
					};
					onCloseCallback = {
						"func": lambda cbSelf: self.sendJSON(mergeDicts({ "type": "resource_gone", "resource": "whatsapp" }, getAttr(cbSelf, "args")), getAttr(cbSelf, "tag")),
						"args": { "resource_instance_id": clientInstanceId }
					};
					self.clientInstances[clientInstanceId] = WhatsAppWebClient(onOpenCallback, onMessageCallback, onCloseCallback);
				else:
					currWhatsAppInstance = self.clientInstances[obj["whatsapp_instance_id"]];
					callback = {
						"func": lambda obj, cbSelf: self.sendJSON(mergeDicts(obj, getAttr(cbSelf, "args")), getAttr(cbSelf, "tag")),
						"tag": tag,
						"args": { "resource_instance_id": obj["whatsapp_instance_id"] }
					};
					if currWhatsAppInstance.activeWs is None:
						self.sendError("No WhatsApp server connected to backend.");
						return;

					cmd = obj["command"];
					if cmd == "backend-generateQRCode":
						currWhatsAppInstance.generateQRCode(callback);
					elif cmd == "backend-restoreSession":
						clientId = getAttr(cmd, "clientId");
						clientToken = getAttr(cmd, "clientToken");
						serverToken = getAttr(cmd, "serverToken");
						currWhatsAppInstance.restoreSession(clientId, clientToken, serverToken, callback);
					elif cmd == "backend-getLoginInfo":
						currWhatsAppInstance.getLoginInfo(callback);
					elif cmd == "backend-getConnectionInfo":
						currWhatsAppInstance.getConnectionInfo(callback);
					elif cmd == "backend-getDeviceInfo":
						currWhatsAppInstance.getDeviceInfo(callback);
					elif cmd == "backend-getStatus":
						currWhatsAppInstance.status(callback);
					elif cmd == "backend-disconnectWhatsApp":
						currWhatsAppInstance.disconnect();
						self.sendJSON({ "type": "resource_disconnected", "resource": "whatsapp", "resource_instance_id": obj["whatsapp_instance_id"] }, tag);
					elif cmd == "backend-sendTextMessage":
						if "text" not in obj or not obj["text"]:
							self.sendError("Text message required!");
							return;
						if "number" not in obj or not obj["number"]:
							self.sendError("Recipient number should be defined!");
							return;
						elif not re.match("\d+", obj["number"]):
							self.sendError("Recipient number format not valid!");
							return;

						currWhatsAppInstance.sendTextMessage(obj["number"], obj["text"]);
						self.sendJSON({ "type": "sendtext" });
		except:
			eprint(traceback.format_exc());

	def handleConnected(self):
		self.sendJSON({ "from": "backend", "type": "connected" });
		eprint(self.address, "connected to backend");

	#def handleConnected(self):
	#	conns.append(self);
	#
	def handleClose(self):
		#conns.remove(self);
		#whatsapp.disconnect();
		self.doClose();

	def doClose(self):
		for k in self.clientInstances:
			self.clientInstances[k].disconnect();

		eprint(self.address, "closed connection to backend");

# Siapkan Signal Handler
signal.signal(signal.SIGINT, keyboardInterruptHandler);

server = SimpleWebSocketServer("", 2020, WhatsAppWeb);
eprint("\x1b[91mwhatsapp-web-backend listening on port 2020\x1b[0m");
server.serveforever();
