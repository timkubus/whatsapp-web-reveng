let _ = require("lodash");
let fs = require("fs");
let path = require("path");
let {StringDecoder} = require("string_decoder");
let express = require("express");
let WebSocket = require("ws");
let app = express();

let {WebSocketClient} = require("./client/js/WebSocketClient.js");
let {BootstrapStep}   = require("./client/js/BootstrapStep.js");



let wss = new WebSocket.Server({ port: 2019 });
console.log("\x1b[91mwhatsapp-web-reveng API server listening on port 2019");

let backendInfo = {
	url: "ws://localhost:2020",
	timeout: 10000
};

wss.on("connection", function(clientWebsocketRaw, req) {
	let backendWebsocket = new WebSocketClient();
	let clientWebsocket = new WebSocketClient().initializeFromRaw(clientWebsocketRaw, "api2client", {getOnMessageData: msg => new StringDecoder("utf-8").write(msg.data)});
	clientWebsocket.send({ type: "connected" });
	/*clientWebsocket.onClose(() => {
		backendWebsocket.disconnect();
		console.log("\x1b[33mBACKEND SOCKET CLOSED");
	});*/

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "api-connectBackend",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(backendWebsocket.isOpen)
			return;
		new BootstrapStep({
			websocket: backendWebsocket,
			actor: websocket => {
				websocket.initialize(backendInfo.url, "api2backend", {func: WebSocket, args: [{ perMessageDeflate: false }], getOnMessageData: msg => new StringDecoder("utf-8").write(msg.data)});
				websocket.onClose(() => {
					clientWebsocket.send({ type: "resource_gone", resource: "backend" });
				});
			},
			request: {
				type: "waitForMessage",
				condition: obj => obj.from == "backend"  &&  obj.type == "connected"
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			clientCallRequest.respond({ type: "resource_connected", resource: "backend" });
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		});
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-connectWhatsApp",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-connectWhatsApp", whatsapp_instance_id: clientCallRequest.data.whatsapp_instance_id },
				successCondition: obj => {
					backendWebsocket._log("\x1b[91m", "Restore Live Session: ", obj);
					return (obj.type == "resource_connected" || obj.type == "resource_reattached")  &&  obj.resource == "whatsapp"  &&  obj.resource_instance_id;
			}}
		}).run(backendInfo.timeout).then(backendResponse => {
			backendWebsocket.activeWhatsAppInstanceId = backendResponse.data.resource_instance_id;
			backendWebsocket.waitForMessage({
				condition: obj => obj.type == "resource_gone"  &&  obj.resource == "whatsapp",
				keepWhenHit: false
			}).then(() => {
				delete backendWebsocket.activeWhatsAppInstanceId;
				clientWebsocket.send({ type: "resource_gone", resource: "whatsapp" });
			});
			let d = { type: backendResponse.data.type, resource: "whatsapp", whatsapp_instance_id: backendResponse.data.resource_instance_id };
			if (backendResponse.data.clientToken)
				d.clientToken = backendResponse.data.clientToken;
			clientCallRequest.respond(d);
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		});
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-disconnectWhatsApp",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-disconnectWhatsApp", whatsapp_instance_id: backendWebsocket.activeWhatsAppInstanceId },
				successCondition: obj => obj.type == "resource_disconnected"  &&  obj.resource == "whatsapp"  &&  obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			clientCallRequest.respond({ type: "resource_disconnected", resource: "whatsapp" });
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		});
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-generateQRCode",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-generateQRCode", whatsapp_instance_id: backendWebsocket.activeWhatsAppInstanceId },
				successCondition: obj => obj.from == "backend"  &&  obj.type == "generated_qr_code"  &&  obj.image  &&  obj.content
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			clientCallRequest.respond({ type: "generated_qr_code", image: backendResponse.data.image })

			backendWebsocket.waitForMessage({
				condition: obj => obj.type == "whatsapp_message_received"  &&  obj.message  &&  obj.message_type  &&  obj.timestamp  &&  obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId,
				keepWhenHit: true
			}).then(whatsAppMessage => {
				let d = whatsAppMessage.data;
				clientWebsocket.send({ type: "whatsapp_message_received", message: d.message, message_type: d.message_type, timestamp: d.timestamp });
			}).run();
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		})
	}).run();


	//TODO:
	// - designated backend call function to make everything shorter
	// - allow client to call "backend-getLoginInfo" and "backend-getConnectionInfo"
	// - add buttons for that to client
	// - look for handlers in "decoder.py" and add them to output information
	// - when decoding fails, write packet to file for further investigation later

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-restoreSession",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: {
					command: "backend-restoreSession", 
					clientId: clientCallRequest.data.clientId, 
					clientToken: clientCallRequest.data.clientToken, 
					serverToken: clientCallRequest.data.serverToken, 
					whatsapp_instance_id: clientCallRequest.data.whatsapp_instance_id ? clientCallRequest.data.whatsapp_instance_id : backendWebsocket.activeWhatsAppInstanceId 
				},
				successCondition: obj => obj.from == "backend"  &&  obj.type == "restore_session"  && obj.data && obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			clientCallRequest.respond({ type: "login_info", data: backendResponse.data.data });
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		})
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-getLoginInfo",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-getLoginInfo", whatsapp_instance_id: backendWebsocket.activeWhatsAppInstanceId },
				successCondition: obj => obj.from == "backend"  &&  obj.type == "login_info"  && obj.data && obj.data.clientId && obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			let d = backendResponse.data.data;
			d.instanceId = backendWebsocket.activeWhatsAppInstanceId;
			clientCallRequest.respond({ type: "login_info", data: d });
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		})
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-getConnectionInfo",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-getConnectionInfo", whatsapp_instance_id: backendWebsocket.activeWhatsAppInstanceId },
				successCondition: obj => obj.from == "backend"  &&  obj.type == "connection_info"  &&  obj.data && obj.data.clientToken && obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			clientCallRequest.respond({ type: "connection_info", data: backendResponse.data.data });
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		})
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-getStatus",
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-getStatus", whatsapp_instance_id: backendWebsocket.activeWhatsAppInstanceId },
				successCondition: obj => obj.from == "backend" && obj.type == "status" && obj.status && obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			clientCallRequest.respond({ type: "status", status: backendResponse.data.status});
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		})
	}).run();

	clientWebsocket.waitForMessage({
		condition: obj => obj.from == "client"  &&  obj.type == "call"  &&  obj.command == "backend-sendTextMessage" && obj.number && obj.text,
		keepWhenHit: true
	}).then(clientCallRequest => {
		if(!backendWebsocket.isOpen) {
			clientCallRequest.respond({ type: "error", reason: "No backend connected." });
			return;
		}
		new BootstrapStep({
			websocket: backendWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-sendTextMessage", whatsapp_instance_id: backendWebsocket.activeWhatsAppInstanceId, text: clientCallRequest.data.text, number: clientCallRequest.data.number },
				successCondition: obj => obj.type == "sendtext"
			}
		}).run(backendInfo.timeout).then(backendResponse => {
			backendWebsocket.waitForMessage({
				condition: obj => obj.type == "whatsapp_message_received" &&  obj.message_type  &&  obj.timestamp  &&  obj.resource_instance_id == backendWebsocket.activeWhatsAppInstanceId
					&&  (obj.message && obj.message.length > 1 && obj.message[0] == "Msg" && obj.message[1].from && obj.message[1].from.replace(/@.+$/, "") == obj.number && obj.message[1].cmd == "ack"),
				keepWhenHit: true
			}).then(whatsAppMessage => {
				let d = whatsAppMessage.data;
				clientWebsocket.send({ type: "sendtext", ack: d.message[1].ack });
			}).run();

			clientCallRequest.respond({ type: "sendtext" });
		}).catch(reason => {
			clientCallRequest.respond({ type: "error", reason: reason });
		});
	}).run();

	/*let send = (obj, tag) => {
		let msgTag = tag==undefined ? (+new Date()) : tag;
		if(obj.from == undefined)
			obj.from = "api";
		clientWebsocket.send(`${msgTag},${JSON.stringify(obj)}`);
	};

	send({ type: "connected" });*/
	/*let backendCall = command => {
		if(!waBackendValid) {
			send({ type: "error", msg: "No backend connected." });
			return;
		}
		waBackend.onmessage = msg => {
			let data = JSON.parse(msg.data);
			if(data.status == 200)
				send(data);
		};
		waBackend.send(command);
	};*/

	/*clientWebsocket.on("message", function(msg) {
		let tag = msg.split(",")[0];
		let obj = JSON.parse(msg.substr(tag.length + 1));

		switch(obj.command) {
			case "api-connectBackend": {*/
				

				//backendWebsocket = new WebSocketClient("ws://localhost:2020", true);
				//backendWebsocket.onClose

				/*waBackend = new WebSocket("ws://localhost:2020", { perMessageDeflate: false });
				waBackend.onclose = () => {
					waBackendValid = false;
					waBackend = undefined;
					send({ type: "resource_gone", resource: "backend" });
				};
				waBackend.onopen = () => {
					waBackendValid = true;
					send({ type: "resource_connected", resource: "backend" }, tag);
				};*/
				//break;
			//}

			/*case "backend-connectWhatsApp":
			case "backend-generateQRCode": {
				backendCall(msg);
				break;
			}*/
	//	}
	//});

	//clientWebsocket.on("close", function() {
		/*if(waBackend != undefined) {
			waBackend.onclose = () => {};
			waBackend.close();
			waBackend = undefined;
		}*/
	//});
})




app.use(express.static("client"));

let server2018 = app.listen(2018, function() {
	console.log("\x1b[91mwhatsapp-web-reveng HTTP server listening on port 2018");
});

process.on('SIGINT', function() {
  console.log("\x1b[33mSIGINT Singal Interrupt!!");
  wss.close();
  server2018.close();
  process.exit();
});