let consoleShown = false;
let apiWebsocket = new WebSocketClient();

function sleep(ms) {
	return new Promise((resolve, reject) => {
		setTimeout(() => resolve(), ms);
	});
}

$(document).ready(function() {
	$("#console-arrow-button").click(() => {
		if(consoleShown) {
			$("#console-arrow").removeClass("extended").find("i.fa").removeClass("fa-angle-right").addClass("fa-angle-left");
			$("#console").removeClass("extended");
		}
		else {
			$("#console-arrow").addClass("extended").find("i.fa").removeClass("fa-angle-left").addClass("fa-angle-right");
			$("#console").addClass("extended");
		}
		consoleShown = !consoleShown;
	});
	
	const responseTimeout = 10000;
	let bootstrapState = 0;
	


	let apiInfo = {
		url: "ws://128.199.88.6:2019",
		timeout: 10000,
		errors: {
			basic: {
				timeout: "Timeout",
				invalidResponse: "Invalid response"
			}
		}
	};



	let allWhatsAppMessages = [];
	let bootstrapInfo = {
		activateButton: (text, buttonEnabled) => {
			let container = $("#bootstrap-container").removeClass("hidden").children("#bootstrap-container-content");
			container.children("img").detach();
			container.children("button").removeClass("hidden").html(text).attr("disabled", !buttonEnabled);
			$("#main-container").addClass("hidden");

			allWhatsAppMessages = [];
			$("#messages-list-table-body").empty();
		},
		activateQRCode: image => {
			let container = $("#bootstrap-container").removeClass("hidden").children("#bootstrap-container-content");
			container.children("button").addClass("hidden")
			container.append($("<img>").attr("src", image));
			$("#main-container").addClass("hidden");
		},
		deactivate: () => {
			$("#bootstrap-container").addClass("hidden");
			$("#main-container").removeClass("hidden");
			$("#button-disconnect").html("Disconnect").attr("disabled", false);
		},
		messageReceived: (websocket) => {
			websocket.waitForMessage({
				condition: obj => obj.type == "whatsapp_message_received"  &&  obj.message,
				keepWhenHit: true
			}).then(whatsAppMessage => {
				bootstrapInfo.deactivate();
				/*<tr>
					<th scope="row">1</th>
					<td>Do., 21.12.2017, 22:59:09.123</td>
					<td>Binary</td>
					<td class="fill no-monospace"><button class="btn">View</button></td>
				</tr>*/

				let d = whatsAppMessage.data;
				let viewJSONButton = $("<button></button>").addClass("btn").html("View").click(function() {
					let messageIndex = parseInt($(this).parent().parent().attr("data-message-index"));
					let jsonData = allWhatsAppMessages[messageIndex];
					let dialog = bootbox.dialog({
						title: `WhatsApp message #${messageIndex+1}`,
						message: "<p>Loading JSON...</p>"
					});
					dialog.init(() => {
						jsonTree.create(jsonData, dialog.find(".bootbox-body").empty()[0]);
					});
				});
				
				let tableRow = $("<tr></tr>").attr("data-message-index", allWhatsAppMessages.length);
				tableRow.append($("<th></th>").attr("scope", "row").html(allWhatsAppMessages.length+1));
				tableRow.append($("<td></td>").html(moment.unix(d.timestamp/1000.0).format("ddd, DD.MM.YYYY, HH:mm:ss.SSS")));
				tableRow.append($("<td></td>").html(d.message_type));
				tableRow.append($("<td></td>").addClass("fill no-monospace").append(viewJSONButton));
				$("#messages-list-table-body").append(tableRow);
				allWhatsAppMessages.push(d.message);

				//$("#main-container-content").empty();
				//jsonTree.create(whatsAppMessage.data.message, $("#main-container-content")[0]);
			}).run();
		},
		steps: [
			new BootstrapStep({
				websocket: apiWebsocket,
				texts: {
					handling: "Connecting to API...",
					success: "Connected to API after %1 ms. Click to let API connect to backend.",
					failure: "Connection to API failed: %1. Click to try again.",
					connLost: "Connection to API closed. Click to reconnect."
				},
				actor: websocket => {
					websocket.initialize(apiInfo.url, "client", {func: WebSocket, getOnMessageData: msg => msg.data});
					websocket.onClose(() => {
						bootstrapInfo.activateButton(bootstrapInfo.steps[0].texts.connLost, true);
						bootstrapState = 0;
					});
				},
				request: {
					type: "waitForMessage",
					condition: obj => obj.type == "connected"
				}
			}),
			new BootstrapStep({
				websocket: apiWebsocket,
				texts: {
					handling: "Connecting to backend...",
					success: "Connected API to backend after %1 ms. Click to let backend connect to WhatsApp.",
					failure: "Connection of API to backend failed: %1. Click to try again.",
					connLost: "Connection of API to backend closed. Click to reconnect."
				},
				actor: websocket => {
					websocket.waitForMessage({
						condition: obj => obj.type == "resource_gone"  &&  obj.resource == "backend",
						keepWhenHit: false
					}).then(() => {
						bootstrapInfo.activateButton(bootstrapInfo.steps[1].texts.connLost, true);
						bootstrapState = 1;
						websocket.apiConnectedToBackend = false;
						websocket.backendConnectedToWhatsApp = false;
					});
				},
				request: {
					type: "call",
					callArgs: { command: "api-connectBackend" },
					successCondition: obj => obj.type == "resource_connected"  &&  obj.resource == "backend",
					successActor: websocket => websocket.apiConnectedToBackend = true
				}
			}),
			new BootstrapStep({
				websocket: apiWebsocket,
				texts: {
					handling: "Connecting to WhatsApp...",
					success: "Connected backend to WhatsApp after %1 ms. Click to generate QR code.",
					failure: "Connection of backend to WhatsApp failed: %1. Click to try again.",
					connLost: "Connection of backend to WhatsApp closed. Click to reconnect."
				},
				actor: websocket => {
					websocket.waitForMessage({
						condition: obj => obj.type == "resource_gone"  &&  obj.resource == "whatsapp",
						keepWhenHit: false
					}).then(() => {
						bootstrapInfo.activateButton(bootstrapInfo.steps[2].texts.connLost, true);
						bootstrapState = 2;
						websocket.backendConnectedToWhatsApp = false;
					});
				},
				request: {
					type: "call",
					callArgs: { command: "backend-connectWhatsApp" },
					successCondition: obj => obj.type == "resource_connected"  &&  obj.resource == "whatsapp",
					successActor: (websocket, obj) => {
						websocket.backendConnectedToWhatsApp = true;
						if (obj.clientToken) {
							bootstrapInfo.clientToken = obj.clientToken;
							bootstrapInfo.messageReceived(websocket);
						}
					},
					timeoutCondition: websocket => websocket.apiConnectedToBackend				//condition for the timeout to be possible at all (if connection to backend is closed, a timeout for connecting to WhatsApp shall not override this issue message)
				}
			}),
			new BootstrapStep({
				websocket: apiWebsocket,
				texts: {
					handling: "Generating QR code...",
					success: "Generated QR code after %1 ms.",
					failure: "Generating QR code failed: %1. Click to try again."
				},
				request: {
					type: "call",
					callArgs: { command: "backend-generateQRCode" },
					successCondition: obj => obj.type == "generated_qr_code"  &&  obj.image,
					successActor: (websocket, {image}) => {
						bootstrapInfo.activateQRCode(image);
						bootstrapInfo.messageReceived(websocket);
					},
					timeoutCondition: websocket => websocket.backendConnectedToWhatsApp
				}
			})
		]
	};

	$("#bootstrap-button").click(function() {
		let currStep = bootstrapInfo.steps[bootstrapState];
		let stepStartTime = performance.now();

		console.log("Fire " + currStep.texts.handling + " Button");

		if (bootstrapState == 2) {
			let instanceId = $('#instanceId').val();
			if (instanceId)
				currStep.request.callArgs.whatsapp_instance_id = instanceId;
		}

		$(this).html(currStep.texts.handling).attr("disabled", "true");
		currStep.run(apiInfo.timeout)
			.then(() => {
				let text = currStep.texts.success.replace("%1", Math.round(performance.now() - stepStartTime));
				$(this).html(text).attr("disabled", false);
				bootstrapState++;
			})
			.catch(reason => {
				let text = currStep.texts.failure.replace("%1", reason);
				$(this).html(text).attr("disabled", false);
			});
	});

	$("#button-disconnect").click(function() {
		if(!apiWebsocket.backendConnectedToWhatsApp)
			return;
		console.log("Fire Disconnect Button");

		$(this).attr("disabled", true).html("Disconnecting...");
		new BootstrapStep({
			websocket: apiWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-disconnectWhatsApp" },
				successCondition: obj => obj.type == "resource_disconnected"  &&  obj.resource == "whatsapp"
			}
		}).run(apiInfo.timeout)
		.then(() => {
			apiWebsocket.backendConnectedToWhatsApp = false;
			$(this).html("Disconnected.");
		}).catch(reason => $(this).html(`Disconnecting failed: ${reason}. Click to try again.`).attr("disabled", false));
	});

	$("#button-restore").click(function() {
		if(!apiWebsocket.backendConnectedToWhatsApp)
			return;

		console.log("Fire Restore Session Button");

		var instanceId = $('#instanceId').val(),
			clientId = $('#clientId').val(),
			clientToken = $('#clientToken').val(),
			serverToken = $('#serverToken').val();
		$(this).attr("disabled", true).html("Restoring session...");
		new BootstrapStep({
			websocket: apiWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-restoreSession", whatsapp_instance_id: instanceId, clientId: clientId, clientToken: clientToken, serverToken: serverToken },
				successCondition: obj => obj.type == "restore_session"  &&  obj.message
			}
		}).run(apiInfo.timeout)
		.then(() => {
			// TODO: lakukan restore

			$(this).html("Restore Session");
		}).catch(reason => $(this).html(`Restoring session failed: ${reason}. Click to try again.`).attr("disabled", false));
	});

	$("#button-info-login").click(function() {
		if(!apiWebsocket.backendConnectedToWhatsApp)
			return;
		
		console.log("Fire Login Info Button");

		$(this).attr("disabled", true).html("Getting login info...");
		new BootstrapStep({
			websocket: apiWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-getLoginInfo" },
				successCondition: obj => obj.type == "login_info"  &&  obj.data
			}
		}).run(apiInfo.timeout)
		.then(() => {
			// TODO: Tampilkan info login
			
			$(this).html('Login Info').attr("disabled", false)
		}).catch(reason => $(this).html(`Getting login info failed: ${reason}. Click to try again.`).attr("disabled", false));
	});

	$("#button-info-connection").click(function() {
		if(!apiWebsocket.backendConnectedToWhatsApp)
			return;
		
		console.log("Fire Connection Info Button");

		$(this).attr("disabled", true).html("Getting connection info...");
		new BootstrapStep({
			websocket: apiWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-getConnectionInfo" },
				successCondition: obj => obj.type == "connection_info"  &&  obj.data
			}
		}).run(apiInfo.timeout)
		.then(() => {
			// TODO: Tampilkan info connection
			
			$(this).html('Connection Info').attr("disabled", false)
		}).catch(reason => $(this).html(`Getting connection info failed: ${reason}. Click to try again.`).attr("disabled", false));
	});

	$("#button-status").click(function() {
		if(!apiWebsocket.backendConnectedToWhatsApp)
			return;

		console.log("Fire Status Button");
		
		$(this).attr("disabled", true).html("Getting status...");
		new BootstrapStep({
			websocket: apiWebsocket,
			request: {
				type: "call",
				callArgs: { command: "backend-getStatus" },
				successCondition: obj => obj.type == "status"  &&  obj.status
			}
		}).run(apiInfo.timeout)
		.then(() => {
			// TODO: Tampilkan status

			$(this).html('Get Status').attr("disabled", false)
		}).catch(reason => $(this).html(`Getting status failed: ${reason}. Click to try again.`).attr("disabled", false));
	});
});
